import logging
import os
import sys
from unittest import TestCase

import pandas as pd
import sqlalchemy

from dBaseOp.dBaseOPs import DatabaseConnection
from dBaseOp.dborm import DB_ORM
from opages.coremain import CoreMain as CM


class TestDBorm(TestCase):

    def test_DBorm(self):
        os.chdir('../')
        dbc = DatabaseConnection()
        # cm = CM()
        assert dbc.relDataPath == '/home/ael-ama/PycharmProjects/icbm-ocean/data/sample/'
        session = dbc.Session()
        dbo = DB_ORM(dbc, session)
        assert dbo is not None
        schema = "io_mass_spec"
        table_name = 'users'
        columns = {"id": "Integer", "name": "String", "email": "String"}
        primary_keys = ["id"]

        tabex = sqlalchemy.inspect(dbc.engine).has_table(table_name=table_name, schema=schema)
        # print('tabex = ', tabex)

        if not tabex:
            classname = dbo.create_table(schema, table_name, columns, primary_keys)
            for i in range(1, 6):
                def_str = {'id': i, 'name': "John C. Doe", 'email': "john.doe@example.com"}
                dbo.add_rows_to_table(classname, def_str)
            dataframe = pd.read_sql_table(schema=schema, table_name=table_name, con=dbc.engine)
            print(dataframe.head())
        else:
            classname = dbo.map_to_class(schema, table_name)
            assert str(type(classname)) == "<class 'sqlalchemy.orm.decl_api.DeclarativeMeta'>"
            try:
                done = dbo.delete_rows_from_table(classname, classname.id == 3)
                dataframe = pd.read_sql_table(schema=schema, table_name=table_name, con=dbc.engine)
                print(dataframe.head())
            except Exception as exception:
                logging.error(f"Failed to query table: {exception}, type: {type(exception)}")
            dbo.drop_table(classname)
        assert isinstance(dbo, DB_ORM)
        dbc.close_session()
