# ICBM-Ocean

## Description

Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Authors and acknowledgment

André El-Ama, CvO-University Oldenburg

## License

[MIT](https://opensource.org/license/mit/)

## Project status

Alpha Academic

## Getting started

### Installation and first start

Then execute these commands:

```sh
git clone git@gitlab.uni-oldenburg.de:icbm-ocean-pydev/icbm-ocean.git
cd icbm-ocean
python -mvenv .venv
. .venv/bin/activate
cp config/config-database.ini config/config-database-privat.ini
streamlit run src/main.py
```

the last command should pop up a browser with the streamlit ICBM-OCEAN version. Some sample files can be found in `data/sample`

### install postgres database or driver (if needed)

**HINT:** Default database is now sqlite, so no additional libs are necessary (this is configured in recent `config-database.ini`). But in case you want to test postgres follow these installation instructions:

Make sure you have postgres installed: Call `pg_config` in shell - should output some compilerflags, if you get a `command not found` just do a

```sh
pip install psycopg2
sudo apt install postgres
# or pacman -S postgres or yum install postgres depending on your linux flavour
```

**Warning:** remember to have venv activated before installing pip package!!!

**Hint:** This installs the full postgres database on your system, if you intend to use a database on a different server it is enough to install `libpq-dev` or `libpq-devel` instead of `postgres`. This will just install the binary driver (including .h, ...) files needed to install the python interface `psycopg2`. Again: for storing data in the database as intended in the current version you need a running postgres instance - either on your machine (install postgres) or on some remote machine (just install libpq)

### Configure postgres database

TODO To be done!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:
