from sqlalchemy import ForeignKey, Float, Integer, String, MetaData
from sqlalchemy.orm import DeclarativeBase, declarative_base
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column


class Base(DeclarativeBase):
    """

    Module: base.py

    This module contains the `Base` class.

    """
    pass


class MassList(Base):
    """
    MassList Class
    ==============

    Class representing a mass list in mass spectrometry analysis.

    Attributes
    ----------
    id : int
        The ID of the mass list.
    m_z : float
        The m/z value of the mass.
    I : int
        The intensity of the mass.
    SN : float
        The signal-to-noise ratio of the mass.
    res : int
        The resolution of the mass.

    Methods
    -------
    __repr__()
        Returns a string representation of the MassList object.

    """
    __tablename__ = 'masslist'
    __table_args__ = {'schema': "io_mass_spec"}

    id: Mapped[int] = mapped_column(primary_key=True)
    m_z: Mapped[float] = mapped_column(Float, name="m/z")
    I: Mapped[int] = mapped_column(Integer)
    SN: Mapped[float] = mapped_column(Float, name="S/N")
    res: Mapped[int] = mapped_column(Integer, name="Res.")

    def __repr__(self) -> str:
        return (f"MassList(id={self.id!r}, "
                f"name={self.m_z!r}, "
                f"fullname={self.I!r}), "
                f"SN={self.SN!r}), "
                f"res={self.res!r})"
                )
