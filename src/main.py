import streamlit as st
from st_pages import Page, Section, show_pages, add_page_title, hide_pages, show_pages_from_config, add_indentation

# Either this or add_indentation() MUST be called on each page in your
# app to add indendation in the sidebar
if not hasattr(st, "ocean_data"):
    st.ocean_data = {
        "title": "ICBM-OCEAN",
        }
# Layout
st.set_page_config(page_title=st.ocean_data['title'], layout="wide", page_icon=":ocean:")
st.title(st.ocean_data['title'])

# add_page_title(hidden_pages=["Home"])
add_indentation()

# list of icons -->  https://streamlit-emoji-shortcodes-streamlit-app-gwckff.streamlit.app/

show_pages_from_config("./config/stPages/pages.toml")
