import sys

import pandas as pd
import numpy as np
from scipy.stats import norm
# from pygam import LinearGAM, s
import pygam as pyg
import plotly.graph_objects as go

import statsmodels.api as sm
import statsmodels.formula.api as smf
import matplotlib.pyplot as plt


class MassSpectrometryAnalyzer:
    def __init__(self, file_paths):
        """
        Initialize the object with the given file paths.

        :param file_paths: A list of file paths to input files.
        """
        self.file_paths = file_paths
        self.data = []
        self.processed_masslists = []
        self.noise_statistics = {}
        pass

    def masspa(self):
        """
        Process the input files.

        :return: A list of pandas DataFrames containing the processed data.
        """
        # Verarbeiten der Eingabedateien
        for file_path in self.file_paths:
            try:
                # Lesen einer CSV-Datei
                data_frame = pd.read_csv(file_path, skipinitialspace=True, header=0,
                                         names=['mz', 'I', 'SN', 'ResPow'],
                                         dtype={'mz': float, 'I': float, 'SN': float, 'ResPow': float},
                                         ).dropna()
                # .columns={'m/z': 'mz', 'I': 'I', 'S/N': 'SN', 'Res.': 'ResPow'}
                # Annahme: Die Spalten 'mz', 'I', 'SN', 'ResPow' müssen vorhanden sein
                # Hier können zusätzliche Datenbereinigungsschritte hinzugefügt werden
                self.data.append(data_frame)
            except Exception as e:
                print(f"Fehler beim Lesen der Datei {file_path}: {e}")
        return self.data

    def masslistdml(self):
        """
        Processes the given mass lists.

        For each mass list in the `data` list, this method performs specific processing steps, such as downsampling,
        outlier removal, etc. The processed data is then appended to the `processed *_masslists` list.

        :return: None
        """
        # Verarbeitung der Massenlisten
        for data_frame in self.data:
            # Hier würden Sie Ihre spezifischen Verarbeitungsschritte durchführen,
            # wie z.B. Downsampling, Bereinigung von Ausreißern, usw.
            processed_data = self.process_data(data_frame)
            self.processed_masslists.append(processed_data)

    def process_data(self, data_frame):
        """
        Process the given data_frame by downsampling it to 50% of its original size.

        :param data_frame: The DataFrame to be processed.
        :return: The downsampled data_frame.
        """
        # Implementieren Sie hier Ihre spezifische Logik zur Datenverarbeitung
        # # Beispiel: Einfaches Downsampling
        # sample_size = int(len(data_frame) * 0.5)  # 50% der Daten
        # downsampled_data = data_frame.sample(n=sample_size)
        # # oder
        downsampled_data = data_frame.sample(frac=0.5)  # 50% der Daten
        return downsampled_data

    def count_noise_peaks(self, nmz, masslist):
        condition_1 = (masslist['mz'] > (nmz[0] - 0.5)) & (masslist['mz'] < (nmz[0] - 0.2))
        condition_2 = (masslist['mz'] > (nmz[0] + 0.4)) & (masslist['mz'] < (nmz[0] + 0.5))
        return sum(condition_1 | condition_2)

    def noise550(self):
        """

        This method calculates the statistics of noise intensity for each processed mass list in
        self.processed_masslists. The statistics are based on the data points that have a mass (m/z) * value within a
        tolerance of 0.1 around 550.

        :return: None

        """
        # Annahme: Jedes DataFrame in self.processed_masslists hat eine 'mz' und 'I' Spalte
        for processed_data in self.processed_masslists:
            # Filtere Daten bei einer Masse von 550 (plus/minus eine Toleranz)
            mz_tolerance = 0.1  # Toleranz für die Masse
            filtered_data = processed_data[(processed_data['mz'] >= 550 - mz_tolerance) &
                                           (processed_data['mz'] <= 550 + mz_tolerance)]

            # Berechne statistische Kennzahlen
            # mean_intensity = filtered_data['mz'].mean()
            # std_intensity = filtered_data['mz'].std()
            mean_intensity = filtered_data['I'].mean()
            std_intensity = filtered_data['I'].std()
            # mean_intensity = filtered_data['SN'].mean()
            # std_intensity = filtered_data['SN'].std()
            # mean_intensity = filtered_data['ResPow'].mean()
            # std_intensity = filtered_data['ResPow'].std()

            # Speichern der Statistiken
            # name = {'mean_intensity': mean_intensity, 'std_intensity': std_intensity}
            self.noise_statistics[processed_data.I.name] = {'mean_intensity': mean_intensity,
                                                            'std_intensity': std_intensity}

    def assess_noise(self, nmz, masslist):
        """
        :param nmz: Value or list/tuple specifying the range of interest for mass
        :param masslist: DataFrame containing mass spectrometry data
        :return: Dictionary containing the statistical metrics of noise

        """
        # nmz ist ein Wert oder eine Liste/Tuple, der/die den interessierenden Massebereich angibt
        # masslist ist ein DataFrame, der die Massenspektrometriedaten enthält

        # Filtere Daten basierend auf dem gegebenen Massebereich
        condition = ((masslist['mz'] > (nmz[0] - 0.5)) & (masslist['mz'] < (nmz[0] - 0.2))) | \
                    ((masslist['mz'] > (nmz[0] + 0.4)) & (masslist['mz'] < (nmz[0] + 0.6)))
        filtered_data = masslist[condition]

        # Berechne die statistischen Kennzahlen
        noise_stats = {
            'Sn.noise': len(filtered_data),
            'SmeanI.noise': filtered_data['I'].mean(),
            'SsdI.noise': filtered_data['I'].std()
        }
        return noise_stats

    def downsample(self, nmz, masslist, thresh_n):
        """
        Filter the masslist based on the given nmz range.

        :param self:
        :param nmz: A tuple representing the range of nmz values.
        :param masslist: A pandas DataFrame containing mass and intensity values.
        :param thresh_n: The maximum number of data points to be included in the downsampled data.
        :return: A downsampled version of the masslist DataFrame.
        """
        # Filtert die Massenliste basierend auf dem gegebenen nmz-Bereich
        condition = ((masslist['mz'] > (nmz[0] - 0.5)) & (masslist['mz'] < (nmz[0] - 0.2))) | \
                    ((masslist['mz'] > (nmz[0] + 0.4)) & (masslist['mz'] < (nmz[0] + 0.5)))

        filtered_masslist = masslist[condition]

        # Bereinigt die Daten durch Entfernen von Ausreißern
        mean_I = filtered_masslist['I'].mean()
        std_I = filtered_masslist['I'].std()
        upper_limit = mean_I + std_I * 3  # Beispielgrenze für Ausreißer

        cleaned_masslist = filtered_masslist[filtered_masslist['I'] < upper_limit]

        # Downsampling der Daten
        sample_size = min(thresh_n, len(cleaned_masslist))
        downsampled_data = cleaned_masslist.sample(n=sample_size)

        return downsampled_data

    def res_pow_outlier(self, dataset):
        """
        :param self: The current instance of a class that contains the method.
        :param dataset: A DataFrame containing columns 'ResPow' and another relevant column.
        :return: The cleaned dataset after removing outliers.

        This method takes a dataset as input and identifies outliers based on a statistical approach, such as the
        Z-Score. It calculates the Z-Score for the 'ResPow' column in the dataset and * adds it as a new column
        called 'Z_Score'. Then, it determines a threshold value for outlier detection. In this example, the threshold
        is set to 3, meaning that any data point with * a Z-Score greater than 3 or less than -3 will be considered
        an outlier. Finally, the method filters out the outliers from the dataset and returns the cleaned dataset.
        """
        # Annahme: dataset ist ein DataFrame mit den Spalten 'ResPow' und einer weiteren relevanten Spalte
        # Erkennung von Ausreißern, z.B. basierend auf einem statistischen Ansatz wie dem Z-Score
        dataset['Z_Score'] = (dataset['ResPow'] - dataset['ResPow'].mean()) / dataset['ResPow'].std()

        # Bestimmen eines Schwellenwerts für die Erkennung von Ausreißern
        threshold = 3  # Beispiel: Z-Score größer als 3 oder kleiner als -3

        # Filtern von Ausreißern
        cleaned_dataset = dataset[(dataset['Z_Score'] < threshold) & (dataset['Z_Score'] > -threshold)]

        return cleaned_dataset

    def snp_masslist(self, dataset, snp_mass_ranges):
        """
        :param self: The instance of the class calling the method
        :param dataset: DataFrame containing mass spectrometry data
        :param snp_mass_ranges: List of tuples representing the mass ranges for SNP

        :return: DataFrame containing SNP mass list

        """
        # dataset ist ein DataFrame mit Massenspektrometriedaten
        # snp_mass_ranges ist eine Liste von Tupeln, die die Massenbereiche für SNP darstellen

        snp_masslist = pd.DataFrame()

        for range_start, range_end in snp_mass_ranges:
            # Filtern des Datensatzes für den spezifischen Massenbereich
            filtered_data = dataset[(dataset['mz'] >= range_start) & (dataset['mz'] <= range_end)]
            snp_masslist = pd.concat([snp_masslist, filtered_data])

        return snp_masslist

    def gam_mdl(self, dataset, x_column, y_column):
        """
        :param self: The instance of the class calling the method.
        :param dataset: The DataFrame with mass spectrometry data.
        :param x_column: The name of the column to be used as the X-axis for the GAM.
        :param y_column: The name of the column to be used as the Y-axis for the GAM.
        :return: A LinearGAM model and the X values used for fitting the model.
        """
        # dataset ist ein DataFrame mit Massenspektrometriedaten
        # x_column und y_column sind die Namen der Spalten, die als X- und Y-Achse für das GAM dienen

        X = dataset[x_column].values
        y = dataset[y_column].values

        # Erstellen eines LinearGAM-Modells
        gam = pyg.LinearGAM(pyg.s(0)).fit(X, y)
        # gam = pyg.ExpectileGAM(pyg.s(0)).fit(X, y)
        # gam = pyg.PoissonGAM(pyg.s(0)).fit(X,y)

        return gam, X, y

    def gam_plot(self, gam, X, y):
        """
        :param self: An instance of the class.
        :param gam: The trained Generalized Additive Model (GAM).
        :param X: The input data for which the predictions are to be made.

        :return: A Plotly figure representing the GAM predictions along with 95% confidence intervals.
        """

        # Generiere eine Reihe von Punkten entlang der X-Achse
        # X_pred = X
        # X_pred = np.linspace(X.min(), X.max(), int(len(X)*0.01))
        X_pred = np.linspace(X.min(), X.max(), 500)
        X_pred = X_pred.reshape(-1, 1)

        # Erzeuge Vorhersagen aus dem GAM für diese Punkte
        y_pred = gam.predict(X_pred)
        y_pred_int = gam.confidence_intervals(X_pred, width=.99)  # 95% Konfidenzintervalle

        # Erstelle das Plotly-Diagramm
        fig = go.Figure()

        # # Originaldaten als Scatter-Plot
        # fig.add_trace(go.Scatter(x=X, mode='markers', name='Daten'))

        # Füge die Vorhersagelinie hinzu
        fig.add_trace(go.Scatter(x=X_pred[:, 0], y=y_pred, mode='lines', name='GAM Vorhersage'))

        # Füge die Konfidenzintervalle hinzu
        fig.add_traces([
            go.Scatter(
                x=np.concatenate([X_pred[:, 0], X_pred[::-1, 0]]),
                y=np.concatenate([y_pred_int[:, 0], y_pred_int[::-1, 1]]),
                fill='toself',
                fillcolor='rgba(0,100,80,0.2)',
                line=dict(color='rgba(255,255,255,0)'),
                name='95% Konfidenzintervall'
            )
        ])

        fig.update_layout(
            title='GAM Vorhersage mit 95% Konfidenzintervallen',
            xaxis_title='X-Achse',
            yaxis_title='Y-Achse'
        )

        return fig

    def quantile_plot(self, data):
        # data = sm.datasets.engel.load_pandas().data
        data = data.sample(n=10000, replace=True, random_state=None)

        mod = smf.quantreg("I ~ mz", data)
        #mod = smf.quantreg("I ~ mz", data)
        # res = mod.fit(q=0.5)
        # print(res.summary())

        # quantiles = np.arange(0.05, 0.96, 0.1)
        quantiles = [0.05, 0.25, 0.5, 0.75, 0.1]

        def fit_model(q):
            res = mod.fit(q=q)
            return [q, res.params["Intercept"], res.params["mz"]] + res.conf_int().loc[
                "mz"
            ].tolist()

        models = [fit_model(x) for x in quantiles]
        models = pd.DataFrame(models, columns=["q", "a", "b", "lb", "ub"])

        ols = smf.ols("I ~ mz", data).fit()
        ols_ci = ols.conf_int().loc["mz"].tolist()
        ols = dict(
            a=ols.params["Intercept"], b=ols.params["mz"], lb=ols_ci[0], ub=ols_ci[1]
        )

        print(models)
        print(ols)

        x = np.arange(data.mz.min(), data.mz.max(), 50)
        get_y = lambda a, b: a + b * x

        fig, ax = plt.subplots(figsize=(8, 6))

        for i in range(models.shape[0]):
            y = get_y(models.a[i], models.b[i])
            ax.plot(x, y, linestyle="dotted", color="grey")

        y = get_y(ols["a"], ols["b"])

        # ax.plot(x, y, color="red", label="OLS")
        # ax.scatter(data.mz, data.I, alpha=0.3, label="data",)
        # # ax.set_xlim((240, 3000))
        # # ax.set_ylim((240, 2000))
        # legend = ax.legend()
        # ax.set_xlabel("mz", fontsize=16)
        # ax.set_ylabel("Intensity", fontsize=16)
        # plt.show()

        n = models.shape[0]
        p0 = plt.scatter(data.mz, data.I, alpha=0.5, label="data", )
        p1 = plt.plot(models.q, models.b, color="black", label="Quantile Reg.")
        p2 = plt.plot(models.q, models.ub, linestyle="dotted", color="black")
        p3 = plt.plot(models.q, models.lb, linestyle="dotted", color="black")
        p4 = plt.plot(models.q, [ols["b"]] * n, color="red", label="OLS")
        p5 = plt.plot(models.q, [ols["lb"]] * n, linestyle="dotted", color="red")
        p6 = plt.plot(models.q, [ols["ub"]] * n, linestyle="dotted", color="red")
        plt.ylabel(r"$\beta_{I}$")
        plt.xlabel("Quantiles of the conditional food expenditure distribution")
        plt.legend()
        plt.show()


if __name__ == '__main__':
    # Beispiel für die Verwendung der Klasse
    file_paths = ["../../data/sample/fakebig-01.csv", "../../data/sample/fakebig-02.csv",
                  "../../data/sample/fakebig-03.csv"]
    analyzer = MassSpectrometryAnalyzer(file_paths)
    analyzer.masspa()
    analyzer.masslistdml()
    analyzer.noise550()
    nmz = [550]  # Beispiel für einen interessierenden Massebereich
    masslist = analyzer.data[0]  # Verwenden der ersten Massenliste als Beispiel
    # noise_statistics = analyzer.assess_noise(nmz, masslist)
    # print(noise_statistics)
    # thresh_n = 100  # Beispiel-Schwellenwert für die Stichprobengröße
    # downsampled_data = analyzer.downsample(nmz, masslist, thresh_n)
    # print(downsampled_data)
    # cleaned_dataset = analyzer.res_pow_outlier(masslist)
    # print(cleaned_dataset)
    # print(analyzer.processed_masslists)
    # print(analyzer.noise_statistics)
    # snp_mass_ranges = [(450, 460), (550, 560)]  # Beispielbereiche für SNP-Massen
    # snp_list = analyzer.snp_masslist(masslist, snp_mass_ranges)

    # gam_model, X, y = analyzer.gam_mdl(masslist, 'mz', 'I')
    # fig = analyzer.gam_plot(gam_model, X, y)
    # fig.show()

    analyzer.quantile_plot(masslist)
