import time

from pygam import LinearGAM, s, l, f


class IOCalc(object):
    def __init__(self):
        pass

    def Calculations(self, x, y):
        # gam = LinearGAM(s(0)).fit(x, y)
        gam = LinearGAM(l(0)).fit(x, y)
        # gam = LinearGAM(f(0)).fit(x, y)
        pred = gam.predict(x)
        conf_int = gam.confidence_intervals(x, width=.95)
        return pred, conf_int
