import os
import subprocess
import xml.etree.ElementTree as ET

import pandas as pd
from ydata_profiling import ProfileReport
import tarfile
import zipfile


class DataIO(object):

    def parse_XML(self, xml_file, df_cols):
        xtree = ET.parse(xml_file)
        xroot = xtree.getroot()
        chanroot = xroot[1][2]
        rows = []

        for node in chanroot:
            res = []
            res.append(node.text)
            rows.append({df_cols[i]: res[i]
                         for i, _ in enumerate(df_cols)})
        datastruct = pd.DataFrame(rows, columns=df_cols)
        return datastruct

    def readData(self, dataStruct, filename):
        # Read the source file that contains sensor data
        df = pd.read_table(filename, dtype=str, delimiter='\t', names=dataStruct[0:].to_numpy().flatten(),
                           na_values=['9', 'format error', 'X'], encoding='iso8859_15', low_memory=False)
        return df

    def store_DF2CSV(self, df, filename):
        df.to_csv(filename)
        return True

    def store_DF2XLS(self, df, filename):
        df.to_excel(filename)
        return True

    def generate_data_profile(self, dataFrame, ReportFlag=False, ProfileReportFile='reports/DataReport.html'):
        # Generate profile report using pandas_profiling
        # type_schema = {"m/z": "Numeric", "I": "Numeric", "S/N": "Numeric", "Res.": "Numeric"}
        # type_schema = {"m/z": "Categorical", "I": "Categorical", "S/N": "Categorical", "Res.": "Categorical"}
        report = ProfileReport(dataFrame, config_file='config/ydata/config-001.yaml')
        report.config.vars.timeseries.active = True
        report.config.title = ProfileReportFile
        if ReportFlag is True:
            DataIO.generate_html_report(self, report, ProfileReportFile)
        return report

    # def generate_data_profile_jupyter(self, dataFrame, ReportFlag=False, ProfileReportFile='reports/DataReportJupyter.html'):
    #     # Generate profile report using pandas_profiling
    #     profile = ProfileReport(dataFrame, dark_mode=True)
    #     # print(report.get_rejected_variables(threshold=0.9))
    #     if ReportFlag is True:
    #         DataIO.generate_jupyter_notebook(self, profile, ProfileReportFile)
    #     return profile

    def generate_html_report(self, profile, ProfileReportFile):
        profile.to_file(ProfileReportFile)
        return True

    # def generate_jupyter_notebook(self, profile, ProfileReportFile):
    #     profile.to_notebook_iframe()
    #     # report.to_widgets()
    #     return True

    def gen_data_description(self, dataFrame, LinRegData, csvname='reports/description.csv',
                             txtname='reports/LinRegReport.txt'):
        """
        :param
        """
        if not os.path.exists('reports'):
            os.makedirs("reports", mode=0o775, exist_ok=True)
        dd = dataFrame.describe()
        dd.to_csv(csvname)
        if os.path.isfile(txtname):
            os.remove(txtname)
        for i in range(0, len(LinRegData)):
            output001 = str(LinRegData[i][0]) + "\n" \
                        + "\t" + "slope: " + str(LinRegData[i][1]) + "\n" \
                        + "\t" + "intercept: " + str(LinRegData[i][2]) + "\n" \
                        + "\t" + "r-value: " + str(LinRegData[i][3]) + "\n" \
                        + "\t" + "p_value: " + str(LinRegData[i][4]) + "\n" \
                        + "\t" + "std_err: " + str(LinRegData[i][5]) + "\n"
            with open(txtname, 'a') as file:
                file.write(output001)
                file.write("\r")
            file.close()

    class impex(object):

        def __init__(self):
            self.InvalidFiles = []

        def writeToIVFList(self):
            f = open("InvalidFiles.txt", "a")
            f.write("IVF: " + self.InvalidFiles.__str__() + '\n')
            f.close()

        def untarFiles(self, tarname):
            returnpath = os.getcwd()
            os.chdir("daily-data/")
            try:
                tar = tarfile.open(tarname)
                tar.extractall()
                tar.close()
            except Exception as e:
                self.InvalidFiles.append(tarname)
                self.writeToIVFList()
                print('ReadError("file could not be opened successfully")')
                return False
            os.chdir(returnpath)
            return True

        def unzipFiles(self, zipname):
            returnpath = os.getcwd()
            os.chdir("tmpdata/")
            try:
                zip = zipfile.ZipFile(zipname)
                zip.extractall()
                zip.close()
            except Exception as e:
                self.InvalidFiles.append(zipname)
                self.writeToIVFList()
                print('Read Error: "File could not be opened successfully or does not exist!"')
                return False
            os.chdir(returnpath)
            return True

        def deleteDataFiles(self):
            returnpath = os.getcwd()
            os.chdir("tmpdata/")
            rmcmd = 'rm -r *'
            try:
                p = subprocess.Popen(rmcmd, shell=True)
                sts = p.wait()
            except Exception as e:
                print('System error' + e.__str__())
                return False
            os.chdir(returnpath)
            return True

        # selected file type extraction
        def untarFilesByExt(self, tarname, ext):
            def ext_Files(members):
                for tarinfo in members:
                    if os.path.splitext(tarinfo.name)[1] == ext:
                        yield tarinfo

            tar = tarfile.open(tarname)
            tar.extractall(members=ext_Files(tar))
            tar.close()


if __name__ == "__main__":
    im = DataIO.impex()
    im.deleteDataFiles()
