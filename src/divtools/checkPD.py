# import sys
import pandas as pd


class DataFrameChecker(object):

    def __init__(self, df: pd.DataFrame):
        self.df = df

    def getNaNs(self):
        sumOfColNaNs = self.get_na_count().to_dict()
        indexOfNaNsColDF: dict = {}
        for cn in self.df.columns:
            indexOfNaNsColDF[cn]: list = []
        indexOfNaNsDF = self.df[self.df.isna().any(axis=1)].index.tolist()
        for i in self.df:
            nan_values_idx = self.df[self.df[i].isna()].index.tolist()
            indexOfNaNsColDF[i].extend(nan_values_idx)
        # print(indexOfNaNsDF)     # Debug
        # print(indexOfNaNsColDF)  # Debug
        # print(sumOfColNaNs)      # Debug
        return indexOfNaNsDF, indexOfNaNsColDF, sumOfColNaNs

    def check_DType(self):
        datatypeOfColDF: dict = {}
        for cn in self.df.columns:
            datatypeOfColDF[cn]: list = []
        for i in self.df:
            if self.df[i].dtypes == int:
                datatypeOfColDF[i] = int
            elif self.df[i].dtypes == float:
                datatypeOfColDF[i] = float
            else:
                datatypeOfColDF[i] = 'Unsupported data type!'
        print(datatypeOfColDF)
        return datatypeOfColDF

    def is_na_exist(self):
        # Check if there are any missing values
        if self.df.isna().sum().sum() > 0:
            return True
        return False

    def get_na_count(self):
        # Get the count of missing values for each column
        return self.df.isna().sum()

    def has_duplicate_rows(self):
        # Check if there are any duplicate rows
        if self.df.duplicated().any():
            return True
        return False

    def get_duplicate_rows_count(self):
        # Get the count of duplicate rows
        return self.df.duplicated().sum()

    def data_integrity_check(self):
        """Perform basic data integrity checks"""
        # Basic checks: no nulls or duplicates
        if self.is_na_exist():
            print("Data has missing values")
        if self.has_duplicate_rows():
            print("Data has duplicate rows")
        # More complex checks can be added here
        else:
            print("Data passed basic integrity checks")


if __name__ == "__main__":
    df = pd.read_csv('your_file.csv')
    checker = DataFrameChecker(df)
    checker.data_integrity_check()
