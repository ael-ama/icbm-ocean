import io
import logging
import sys
import time
from io import StringIO

import numpy as np
import pandas as pd
import dask.dataframe as dd
from dBaseOp.provar import ProVars as pv
import streamlit as st


class IOFunc(pv, object):
    """
    Reads a CSV file and returns the DataFrame along with x and y values.

    :param filename: The path to the CSV file.
    :type filename: str
    :return: A tuple containing the DataFrame, x values, and y values.
    :rtype: tuple
    """

    def __init__(self):
        """
        Initialize the object.

        :param self: The object instance.
        """
        super().__init__()
        # self.filename = filename
        pass

    def setupPath(self, filename):
        file = self.relDataPath + filename
        return file

    @st.cache_data
    def get_CSV_UploadData(_self, filename, sample_size=1000, frac=0.00):
        """
        Pandas Dataframe solution
        :param frac:
        :param filename: The path or name of the CSV file to be read and sampled.
        :param sample_size: The number of random samples to return from the CSV file.
        :return: A tuple containing the sampled DataFrame, and the x and y arrays.
        """
        data_frame = pd.DataFrame()
        if sample_size is None:
            data_frame = pd.read_csv(IOFunc().setupPath(filename),
                                     skipinitialspace=True).sample(frac=frac, replace=True, random_state=None)
        else:
            try:
                data_frame = pd.read_csv(IOFunc().setupPath(filename), skipinitialspace=True)
                if len(data_frame) > sample_size:
                    data_frame = data_frame.sample(n=sample_size, replace=True, random_state=None)
                else:
                    pass
            except pd.errors.DataError as exception:
                logging.error(f"Failed to query table: {exception}, type: {type(exception)}")
                # raise exception
        return data_frame
