import csv
import math
import multiprocessing
import re
from io import StringIO
from typing import Optional, Tuple

import more_itertools
import numpy as np
import pandas as pd
import shapely
from more_itertools import flatten

from sklearn.preprocessing import MinMaxScaler


class Tools(object):
    """ multiple processes """

    def __init__(self):
        pass

    def getNuberOfProcesses(self, numOfVars):
        if numOfVars > multiprocessing.cpu_count():
            # processes = numOfVars
            # processes = (numOfVars // multiprocessing.cpu_count()) - 1
            # processes = (numOfVars // multiprocessing.cpu_count()) * multiprocessing.cpu_count()
            processes = multiprocessing.cpu_count() - 1
        else:
            processes = multiprocessing.cpu_count() - 1
        # print(processes)
        return processes

    def createProcessPool(self, numOfVars):
        processes = self.getNuberOfProcesses(numOfVars)
        processPool = multiprocessing.Pool(processes=processes)
        return processPool

    def normalizeDataSKLearn(self, df, a, b):
        values = np.asarray(df.values, dtype=np.float64)
        scaler = MinMaxScaler(feature_range=(a, b))
        scaler = scaler.fit(values)
        normalized = scaler.transform(values)
        return pd.DataFrame(normalized)

    def normalizeData(self, df, a, b):
        # df = df.apply(lambda x: ((b - a) * ((x - np.min(x)) / (np.max(x) - np.min(x))) + a), axis=1)
        df = df.apply(lambda x: ((b - a) * ((x - np.min(x)) / (np.max(x) - np.min(x))) + a), axis=0)
        return df

    # Alternative to_sql() *method* for DBs that support COPY FROM
    def psql_insert_copy(self, table, conn, keys, data_iter):
        """
        Execute SQL statement inserting data

        Parameters
        ----------
        table : pandas.io.sql.SQLTable
        conn : sqlalchemy.engine.Engine or sqlalchemy.engine.Connection
        keys : list of str
            Column names
        data_iter : Iterable that iterates the values to be inserted
        """
        # gets a DBAPI connection that can provide a cursor
        dbapi_conn = conn.connection
        with dbapi_conn.cursor() as cur:
            s_buf = StringIO()
            writer = csv.writer(s_buf)
            writer.writerows(data_iter)
            s_buf.seek(0)

            columns = ', '.join('"{}"'.format(k) for k in keys)
            if table.schema:
                table_name = '{}.{}'.format(table.schema, table.name)
            else:
                table_name = table.name

            sql = 'COPY {} ({}) FROM STDIN WITH CSV'.format(
                table_name, columns)
            cur.copy_expert(sql=sql, file=s_buf)

    def dms2dd(self, s):
        # example: s = """15° 57.022432' N"""
        if s is not None:
            degrees, minutes, direction = re.split("[° ']+", s)
            dd = float(degrees) + float(minutes) / 60
            if direction in ('S', 'W'):
                dd *= -1
        else:
            dd = None
        return dd

    def lat_gdf2dg(self, latitude, longitude):
        # Latitude
        N = 'N' in latitude
        # print(latitude[:-2].split(' '))
        d, m = map(float, latitude[:-2].split(' '))
        latitude = (d + m / 60) * (1 if N else -1)
        # Longitude
        W = 'W' in longitude
        # print(longitude[:-2].split(' '))
        d, m = map(float, longitude[:-2].split(' '))
        longitude = (d + m / 60) * (-1 if W else 1)
        return latitude, longitude

    def gdm2dg(self, lat, lon):

        lat.replace(to_replace=r"'", value='', regex=True, inplace=True)
        lat.replace(to_replace=r"°", value='', regex=True, inplace=True)
        lon.replace(to_replace=r"'", value='', regex=True, inplace=True)
        lon.replace(to_replace=r"°", value='', regex=True, inplace=True)

        listOfdgLat = []
        listOfdgLon = []
        numOfIter = len(lat)

        for i in range(0, numOfIter):
            if lat[i] or lon[i] is not None:
                dgLat, dgLon = self.lat_gdf2dg(lat[i], lon[i])
                listOfdgLat.append(dgLat)
                listOfdgLon.append(dgLon)
            else:
                listOfdgLat.append(None)
                listOfdgLon.append(None)

        return listOfdgLat, listOfdgLon

    # good
    def rolling_window(self, iterable, n, s=1):
        result = list(more_itertools.windowed(iterable, n=n, step=s))
        return result

    # bad
    # def rolling_window(self, seq, window_size):
    #     it = iter(seq)
    #     if len(seq) != 0:
    #         win = [it.__next__() for cnt in xrange(window_size)]
    #         yield win
    #         for e in it:  # Subsequent windows
    #             win[:-1] = win[1:]
    #             win[-1] = e
    #             yield win
    #     else:
    #         yield []

    # good
    # def rolling_window(self, seq, window_size=5):
    #     """
    #     Returns a sliding window (of width n) over data from the iterable
    #     s -> (s0,s1,...s[n-1]), (s1,s2,...,sn), ...
    #     """
    #     it = iter(seq)
    #     result = tuple(islice(it, window_size))
    #     if len(result) == window_size:
    #         yield result
    #     for elem in it:
    #         result = result[1:] + (elem,)
    #         yield result

    def split_df(self, df, df_size):
        cnt = 0
        dict_df = {}
        int_div = math.trunc(len(df) / df_size)
        rest_div = len(df) % df_size
        if int_div == 1 and rest_div == 0:
            dict_df[1] = df.iloc[(cnt * df_size):(1 * df_size), :]
            cnt = 1
        else:
            for i in range(1, int_div + 1):
                dict_df[i] = df.iloc[(cnt * df_size):(i * df_size), :]
                cnt = i
            dict_df[cnt + 1] = df.iloc[(cnt * df_size):, :]
        return dict_df, cnt

    def flatten(self, d):
        res = []  # type:list  # Result list
        if isinstance(d, dict):
            for key, val in d.items():
                res.extend(flatten(val))
        elif isinstance(d, list):
            for val in d:
                res.extend(more_itertools.flatten(val))
        elif isinstance(d, float):
            res = [d]  # type: List[float]
        elif isinstance(d, str):
            res = [d]  # type: List[str]
        elif isinstance(d, int):
            res = [d]  # type: List[int]
        else:
            raise TypeError("Undefined type for flatten: %s" % type(d))
        return res

    def valid_lonlat(self, lon: float, lat: float) -> Optional[Tuple[float, float]]:
        from shapely.geometry import Point
        """
        This validates a lat and lon point can be located
        in the bounds of the WGS84 CRS, after wrapping the
        longitude value within [-180, 180)

        :param lon: a longitude value
        :param lat: a latitude value
        :return: (lon, lat) if valid, None otherwise
        """
        # Put the longitude in the range of [0,360):
        lon %= 360
        # Put the longitude in the range of [-180,180):
        if lon >= 180:
            lon -= 360
        lon_lat_point = shapely.geometry.Point(lon, lat)
        lon_lat_bounds = shapely.geometry.Polygon.from_bounds(
            xmin=-180.0, ymin=-90.0, xmax=180.0, ymax=90.0
        )
        # return lon_lat_bounds.intersects(lon_lat_point)
        # would not provide any corrected values

        if lon_lat_bounds.intersects(lon_lat_point):
            return lon, lat
        else:
            return None
