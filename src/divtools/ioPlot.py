import numpy as np
import streamlit as st
import pandas as pd
import time

import plotly.graph_objects as go
from dBaseOp.provar import ProVars as pv


class IOPlot(pv, object):
    """
    Get a list of CSV files in a given directory.

    :param directory_name: The path of the directory.
    :type directory_name: str
    :return: List of CSV files in the directory.
    :rtype: List[str]
    """
    def __init__(self):
        super().__init__()
        pass

    def readDir(self):
        """
        Fetches the names of all CSV files within a specified directory.

        :param directory_name: The path of the directory to search for CSV files.
        :return: A list of CSV file names within the specified directory.
        """
        import glob
        import os
        CSVFiles = []
        for file in glob.glob(glob.escape(self.relDataPath) + "/*.csv"):
            CSVFiles.append(os.path.basename(file))
        return CSVFiles

    def FileUploader(self, key='def_key'):
        """
        FileUploader method allows the user to upload a CSV file.

        :return: The uploaded file object if a file is selected, otherwise None.
        """
        # st.sidebar.title("Upload the data file:")
        uploaded_files = st.sidebar.file_uploader("Upload a CSV file", accept_multiple_files=False, type=['csv', 'txt'],
                                                  key=key)
        if uploaded_files is not None:
            return uploaded_files
        else:
            return uploaded_files

    def LocalUploader(self, key):
        """
        :param key: The key to be used for the selectbox in the sidebar.
        :param directory_name: The name of the directory containing the files to be uploaded.
        :return: The list of uploaded local files.

        """
        local_files = []
        csvfiles = self.readDir()
        local_files.append(st.sidebar.selectbox(key=key, label="Uploaded local files", options=csvfiles))
        if local_files is not None:
            return local_files
        else:
            return local_files

    def Config_plot(self, x, y, pred, conf_int):
        """
        :param x: List of x values for the scatter plot.
        :param y: List of y values for the scatter plot.
        :param pred: List of predicted values for the line plot.
        :param conf_int: 2D array representing the confidence intervals for the fill plot.
        :return: Figure object representing the configured plot.

        This method configures a plot using the plotly library. It takes in four parameters: x, y, pred, and conf_int.
        The x parameter is a list of x values for the scatter plot. The y parameter is a list of y values for the scatter plot.
        The pred parameter is a list of predicted values for the line plot. The conf_int parameter is a 2D array representing
        the confidence intervals for the fill plot.

        The method creates a Figure object and adds three traces to it:
        - A scatter plot trace using the x and y values.
        - A line plot trace using the x and pred values.
        - A fill plot trace using the x, conf_int[:, 1], and conf_int[:, 0][::-1] values.

        The method also updates the layout of the figure with a title, x-axis title, y-axis title, and legend settings.

        Finally, the method returns the configured figure.
        """
        fig = go.Figure()

        # # Originaldaten als Scatter-Plot
        fig.add_trace(go.Scatter(x=x, y=y, mode='markers', name='Daten'))

        # GAM Vorhersage
        # fig.add_trace(go.Scatter(x=x, y=pred, mode='markers', name='GAM Vorhersage'))
        fig.add_trace(go.Scatter(x=x, y=pred + np.log(2.0), mode='markers', marker_size=3, marker_symbol=1,
                                 name='GAM Vorhersage'))
        fig.add_trace(go.Scatter(x=x, y=pred + np.log(2.5), mode='markers', marker_size=3, marker_symbol=1,
                                 name='GAM Vorhersage'))
        fig.add_trace(go.Scatter(x=x, y=pred + np.log(3.0), mode='markers', marker_size=3, marker_symbol=1,
                                 name='GAM Vorhersage'))
        # Konfidenzintervalle
        fig.add_trace(go.Scatter(x=np.concatenate([x, x[::-1]]),
                                 y=np.concatenate([conf_int[:, 1], conf_int[:, 0][::-1]]),
                                 fill='toself', fillcolor='rgba(0,100,80,0.2)',
                                 line=dict(color='rgba(255,255,255,0)'),
                                 name='95% Konfidenzintervall'))

        # Layout-Einstellungen
        fig.update_layout(title='GAM Modellierung der Intensitätswerte',
                          xaxis_title='m/z',
                          yaxis_title='Intensität',
                          legend=dict(y=0.5, traceorder='reversed', font_size=16),)
        # Diagramm anzeigen
        # fig.show()  # Debug purpose
        return fig

    def config_plotColumn(self, x, y, color='blue', title='Scatter plot', x_title='X-axis',
                          y_title='Y-axis'):
        """
        Plot a single column of data from a dataframe using Plotly
        :param x: The name of the column to be plotted along the x-axis.
        :param y: The name of the column to be plotted along the y-axis.
        :param color: The color of the points in the scatter plot. Default is 'blue'.
        :param title: The title of the plot. Default is 'Scatter plot'.
        :param x_title: The title of the x-axis. Default is 'X-axis'.
        :param y_title: The title of the y-axis. Default is 'Y-axis'.
        :return: The plotly figure
        """
        fig = go.Figure()
        try:
            # Create a figure using Plotly Express
            fig.add_trace(go.Scatter(x=x, y=y, mode='markers', name='Daten'))

            # Adjust layout features
            fig.update_layout(title=title,
                              xaxis_title=x_title,
                              yaxis_title=y_title,
                              )
            # Return the figure
            return fig
        except (KeyError, TypeError) as error:
            print(f"Error in plotting column '{x, y}': {error}")

    def ShowPlot(self, tab, fig):
        """
        Shows an application in a tab with a plotly chart and a formatted dataframe.

        :param tab: The tab element where the application will be displayed.
        :type tab: Streamlit tab element

        :param df: The dataframe to be displayed.
        :type df: pandas DataFrame

        :param fig: The plotly figure to be displayed.
        :type fig: plotly Figure

        :return: None
        """
        # tab.empty()
        # tab.write(fig)
        p_obj = tab.plotly_chart(fig, theme="streamlit", use_container_width=True)
        # tab.dataframe(df.round(3).head(100).style.format(decimal=",", thousands='', precision=4), use_container_width=True, hide_index=True)
        # tab.dataframe(df.style.format(decimal=".", thousands='', precision=4), use_container_width=True, hide_index=True)
        return p_obj

    def ShowApp(self, tab, df, fig):
        """
        Shows an application in a tab with a plotly chart and a formatted dataframe.

        :param tab: The tab element where the application will be displayed.
        :type tab: Streamlit tab element

        :param df: The dataframe to be displayed.
        :type df: pandas DataFrame

        :param fig: The plotly figure to be displayed.
        :type fig: plotly Figure

        :return: None
        """
        # tab.empty()
        # tab.write(fig)
        tab.plotly_chart(fig, theme="streamlit", use_container_width=True)
        tab.dataframe(df.round(3).head(100).style.format(decimal=",", thousands='', precision=4), use_container_width=True, hide_index=True)
        # tab.dataframe(df.style.format(decimal=".", thousands='', precision=4), use_container_width=True, hide_index=True)
