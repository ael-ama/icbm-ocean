import streamlit.components.v1 as components
import streamlit as st
import extra_streamlit_components as stx
from opages.coremain import CoreMain as CM
import streamlit_extras.dataframe_explorer as dfe
import datetime as dt

cm = CM()


def gen_db_name(filename):
    df = cm.upLoadAll(filename, frac=1.0)
    t_stamp = dt.datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
    # tab_name = "masslist_" + uploaded_files.name.replace(".", "_") + "_" + t_stamp
    tab_name = filename.replace(".", "_") + "_" + t_stamp
    return tab_name, df


def store_ToDatabase(filename):
    # df = cm.upLoadAll(filename, frac=1.0)
    # t_stamp = dt.datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
    # # tab_name = "masslist_" + uploaded_files.name.replace(".", "_") + "_" + t_stamp
    # tab_name = filename.replace(".", "_") + "_" + t_stamp
    tab_name, df = gen_db_name(filename)
    cm.storeCSV_UploadData(df, tab_name, cm.pv.I_DB_SCHE)
    st.info("Stored data from file '{}' with rows {}".format(file_name, str(len(df))))


def upload():

    global udf, filtered_df, obj, file_name

    col_scat_plots = []
    figs = []

    if 'store_all' not in st.session_state:
        st.session_state['store_all'] = False

    st.set_page_config(page_title=f"Upload :: {st.ocean_data['title']}",layout="wide", page_icon=":ocean:")

    st.subheader("Upload data:")

    chosen_id = stx.tab_bar(data=[
        stx.TabBarItemData(id="tab1", title="Table view", description="Uploded file"),
        stx.TabBarItemData(id="tab2", title="Chart view", description="Uploaded file"),
        stx.TabBarItemData(id="tab3", title="Store to Databasee", description="Store files to DB"),
        # stx.TabBarItemData(id="tab4", title="Report", description="Generate report")
    ])

    sample_size = st.sidebar.text_input('Sample size:', value=1000, key='ss_1')
    udf, file_name = cm.upLoad(sample_size=int(sample_size), frac=0.0, key='up_01')
    # if file_name:
    #     t_stamp = dt.datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
    #     # tab_name = "masslist_" + uploaded_files.name.replace(".", "_") + "_" + t_stamp
    #     tab_name = file_name.replace(".", "_") + "_" + t_stamp
    #     cm.storeCSV_UploadData(udf, tab_name, cm.pv.I_DB_SCHE)
    if udf is not None:
        if chosen_id == "tab3":
            pass
        else:
            filtered_df = dfe.dataframe_explorer(udf)
            if st.session_state.store_all:
                # st.warning("Selected data rows = Sample size")
                if len(filtered_df) > int(sample_size):
                    st.sidebar.write("Selected data rows: ", int(sample_size))
                else:
                    st.sidebar.write("Selected data rows: ", len(filtered_df))
            else:
                st.sidebar.write("Selected data rows: ", len(filtered_df))

    uploads = st.container()

    if chosen_id == "tab1":
        if udf is not None:
            uploads.dataframe(filtered_df.round(3).head(1000).style.format(decimal=",", thousands='', precision=4),
                              use_container_width=True, hide_index=True)
            vcb = st.sidebar.checkbox(label="Store whole sample size:", key='store_all')
            if vcb is True:
                stored = st.sidebar.button(label="Store sample to DB:", key='store_button',
                                           on_click=cm.storeCSV_UploadData,
                                           args=[udf, file_name, 'io_mass_spec'])
            else:
                stored = st.sidebar.button(label="Store sample to DB:", key='store_button',
                                           on_click=cm.storeCSV_UploadData,
                                           args=[filtered_df, file_name, 'io_mass_spec'])
            if stored:
                # st.cache_data.clear()
                st.sidebar.write('Stored in database!')
        else:
            pass

    elif chosen_id == "tab2":
        if udf is not None:
            # x, y, pred, conf_int = cm.call_GAMCalc(filtered_df)
            px, py = cm.call_ColumnXY(filtered_df)
            for i_vars in range(len(py)):
                pfig = cm.call_scatterPlot_config(px[i_vars], py[i_vars], filtered_df.columns[i_vars])
                col_scat_plots.append(pfig)
        else:
            filtered_df = None
        if col_scat_plots is not None and udf is not None:
            for i_figs in range(len(col_scat_plots)):
                obj = cm.iop.ShowPlot(uploads, col_scat_plots[i_figs])
            tab_name, df = gen_db_name(file_name)
            vcb = st.sidebar.checkbox(label="Store whole sample size:", key='store_all')
            if vcb is True:
                stored = st.sidebar.button(label="Store sample to DB:", key='store_button',
                                           on_click=cm.storeCSV_UploadData,
                                           args=[udf, tab_name, 'io_mass_spec'])
            else:
                stored = st.sidebar.button(label="Store sample to DB:", key='store_button',
                                           on_click=cm.storeCSV_UploadData,
                                           args=[filtered_df, tab_name, 'io_mass_spec'])
            if stored:
                # st.cache_data.clear()
                st.sidebar.write('Stored in database!')

    elif chosen_id == "tab3":
        if file_name:
            uploads.button(label="Store whole file to DB:", key='store_file_button',
                           on_click=store_ToDatabase,
                           args=[file_name])
        else:
            pass

    else:
        pass


if __name__ == '__main__':
    upload()
