import logging

import streamlit.components.v1 as components
import streamlit as st
import extra_streamlit_components as stx
from opages.coremain import CoreMain as CM


class Report(object):

    def __init__(self):
        self.cm = CM()

    def report(self):
        cm = CM()
        st.set_page_config(page_title=f"Reports :: {st.ocean_data['title']}",  layout="wide", page_icon=":ocean:")
        reports = st.container()
        st.sidebar.title("Generate Report:")
        ldf, file = cm.localLoad('nice', sample_size=int(st.sidebar.text_input('sample_size', value='1000', key='ss_4')),
                                 frac=None)
        if file:
            reports.subheader("".join(["Statistical Profile: ", file]))
            # rep_name = "data/data_report.html"
            rep_name = "reports/" + file.replace(".", "_") + "_report.html"
            print(rep_name)
            st.sidebar.button(label="Generate:", key='gen_button', on_click=cm.dio.generate_data_profile,
                              args=[ldf, True, rep_name])
            try:
                htmlFile = open(rep_name, 'r', encoding='utf-8')
                source_code = htmlFile.read()
                components.html(source_code, height=800, scrolling=True)
            except FileNotFoundError as IOE:
                logging.error(f"Failed to store CSV data: {IOE}, type: {type(IOE)}")
                # st.write(ioe)
                reports.empty()
        else:
            reports.subheader("".join(["Statistical Profile: ", None]))
            reports.empty()
        pass


if __name__ == '__main__':
    rp = Report()
    rp.report()
