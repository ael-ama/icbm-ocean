import streamlit.components.v1 as components
import streamlit as st
import extra_streamlit_components as stx
from numpy import log

from opages.coremain import CoreMain as CM
import streamlit_extras.dataframe_explorer as dfe
import datetime as dt


class DataView(object):
    """

    :class:`DataView` is a class that provides functionality for viewing data in different ways.

    """
    def __init__(self):
        self.cm = CM()

    def dataview(self):
        """
        This method is used to display and interact with data in the application.

        :return: None
        """
        global udf, filtered_df, obj, file_name

        st.set_page_config(page_title=f"DataView :: {st.ocean_data['title']}", layout="wide", page_icon=":ocean:")

        st.subheader("View data:")

        chosen_id = stx.tab_bar(data=[
            stx.TabBarItemData(id="tab1", title="Graph view", description="data"),
            # stx.TabBarItemData(id="tab2", title="Graph view", description="data"),
            # stx.TabBarItemData(id="tab3", title="Graph view", description="data"),
            # stx.TabBarItemData(id="tab4", title="Report", description="Generate report")
        ])

        sample_size = st.sidebar.text_input('Sample size:', value=1000, key='ss_dataview_0')
        udf, file_name = self.cm.upLoad(sample_size=int(sample_size), frac=0.0, key='ss_dvup_1')

        dataview = st.container()

        if chosen_id == "tab1":
            dataview.subheader("Upload data chart")
            # udf, filename = self.cm.upLoad(sample_size=int(st.sidebar.text_input('sample_size', value=1000, key='ss_dataview_1')),
            #                           frac=None, key='ss_dvup_2')
            if udf is not None:
                x, y, pred, conf_int = self.cm.call_GAMCalc(udf)
                # ufig = self.cm.call_ConfigPlot(x, y, pred, conf_int)
                ufig = self.cm.call_ConfigPlot(log(x, where=x > 0.001), log(y, where=y > 0.001),
                                               log(pred, where=pred > 0.001), log(conf_int, where=conf_int > 0.001))
            else:
                ufig = None
            # udf, ufig = localLoad(frac=float(st.sidebar.text_input('frac', value=0.01, key='ss_0')), key='ss_1')
            if ufig is not None and udf is not None:
                self.cm.iop.ShowApp(dataview, udf, ufig)
                # # createTable()
                # stored = st.sidebar.button(label="Store into DB:", key='store_button', on_click=self.cm.storeCSV_UploadData,
                #                            args=[udf, 'masslist', 'io_mass_spec'])
                # if stored:
                #     # st.cache_data.clear()
                #     st.sidebar.write('Stored in database!')
                # else:
                #     pass

        elif chosen_id == "tab2":
            if udf is not None:
                pass
            else:
                filtered_df = None
                pass

        elif chosen_id == "tab3":
            if file_name:
                pass
            else:
                pass

        else:
            pass


if __name__ == '__main__':
    dv = DataView()
    dv.dataview()
