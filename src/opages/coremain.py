# import importlib
import logging
import sys
import time
from tkinter import filedialog

import numpy as np
import pandas as pd
from sqlalchemy.exc import DatabaseError, OperationalError
import streamlit.components.v1 as components
from st_pages import add_indentation

import dBaseOp.provar

from divtools.ioFunc import IOFunc as IO
from ioCalc.divCalc import IOCalc as IOC
from divtools.ioPlot import IOPlot as IOP
from dBaseOp.dborm import DB_ORM as DO
from dBaseOp.provar import ProVars as PV
from divtools.IOData import DataIO as DIO

import streamlit as st
from streamlit_extras.dataframe_explorer import dataframe_explorer
import extra_streamlit_components as stx
from dBaseOp.dBaseOPs import DatabaseConnection, dbBasicOperations

import dask.array as da


class CoreMain(object):

    def __init__(self):
        self.fio = IO()
        self.iop = IOP()
        self.ioc = IOC()
        self.pv = PV()
        self.dio = DIO()

    def call_GAMCalc(self, df):
        print(df.head(5))
        x = df[self.pv.tableColumn_1].values
        y = df[self.pv.tableColumn_2].values
        pred, conf_int = self.ioc.Calculations(x, y)
        return x, y, pred, conf_int

    def call_ColumnXY(self, df):
        x, y, = [], []
        y1 = df[df.columns[0]].values
        # y1 = df[self.pv.tableColumn_1].values
        x1 = np.arange(0, len(y1))
        x.append(x1)
        y.append(y1)
        y2 = df[df.columns[1]].values
        # y2 = df[self.pv.tableColumn_2].values
        x2 = np.arange(0, len(y2))
        x.append(x2)
        y.append(y2)
        y3 = df[df.columns[2]].values
        # y3 = df[self.pv.tableColumn_3].values
        x3 = np.arange(0, len(y3))
        x.append(x3)
        y.append(y3)
        y4 = df[df.columns[3]].values
        # y4 = df[self.pv.tableColumn_4].values
        x4 = np.arange(0, len(y4))
        x.append(x4)
        y.append(y4)
        return x, y

    def call_scatterPlot_config(self, x, y, c_name=None):
        fig = self.iop.config_plotColumn(x, y, title="Scatter plot " + c_name)
        return fig

    def call_ConfigPlot(self, x, y, pred, conf_int):
        fig = self.iop.Config_plot(x, y, pred, conf_int)
        return fig

    def localLoad(self, key, sample_size, frac):
        local_files = self.iop.LocalUploader(key)
        if sample_size == 0:
            sample_size = None
        if len(local_files) != 0:
            df = self.fio.get_CSV_UploadData(local_files[0], sample_size, frac)
            # x, y, pred, conf_int = call_GAMCalc(df)
            # fig = call_ConfigPlot(x, y, pred, conf_int)
            return df, local_files[0]

    def upLoad(self, sample_size, frac, key):
        uploaded_files = self.iop.FileUploader(key=key)
        if sample_size == 0:
            sample_size = None
        if uploaded_files is not None:
            df = self.fio.get_CSV_UploadData(uploaded_files.name, sample_size, frac)
            # x = df_t2[pv.tableColumn_1].values
            # y = df_t2[pv.tableColumn_2].values
            # pred, conf_int = ioc.Calculations(x, y)
            # fig = iop.Config_plot(x, y, pred, conf_int)
            return df, uploaded_files.name
        else:
            return None, None

    def upLoadAll(self, filename, frac):
        if filename is not None:
            df = self.fio.get_CSV_UploadData(filename, sample_size=None, frac=frac)
            # x = df_t2[pv.tableColumn_1].values
            # y = df_t2[pv.tableColumn_2].values
            # pred, conf_int = ioc.Calculations(x, y)
            # fig = iop.Config_plot(x, y, pred, conf_int)
            return df
        else:
            return None, None

    def storeCSV_UploadData(self, dataframe, tableName, schema):
        """
        Store CSV data in database.

        :param dataframe: The dataframe containing the csv data.
        :param tableName: The name of the table to store the data in.
        :param schema: The schema of the table.
        :return: None
        """
        try:
            dbc = DatabaseConnection()
            session = dbc.Session()
            dbo = DO(dbc, session)
            dbo.store_in_DB(dataframe, tableName, schema=schema, idxName='id')
            dbo.close_session()
        except OperationalError as Con_Exception:
            logging.error(f"Failed to store CSV data: {Con_Exception}, type: {type(Con_Exception)}")
            error = ("Ups... a Database Error occurred! {0}".format(Con_Exception))
            st.sidebar.write(error)

    def createTable(self):
        from dataModels.masslists import MassList, Base
        try:
            dbc = DatabaseConnection()
            session = dbc.Session
            dbo = DO(dbc, session)
            Base.metadata.create_all(dbo.engine, checkfirst=True)
            dbc.close_session()
        except OperationalError as Con_Exception:
            logging.error(f"Failed to store CSV data: {Con_Exception}, type: {type(Con_Exception)}")
            error = ("Ups... a Database Error occurred! {0}".format(Con_Exception))
            st.sidebar.write(error)
