import streamlit as st
import streamlit.components.v1 as components
import streamlit_extras.dataframe_explorer as dfe
import extra_streamlit_components as stx
from opages.coremain import CoreMain as CM


def data_select():
    global pfig
    cm = CM()
    col_scat_plots = []

    st.set_page_config(page_title=f"DataSelect :: {st.ocean_data['title']}", layout="wide", page_icon=":ocean:")

    ldf, file = cm.localLoad('nice',
                             sample_size=int(st.sidebar.text_input('sample_size', value='1000', key='ss_4')),
                             frac=None)
    filtered_df = dfe.dataframe_explorer(ldf)
    select = st.container()

    if filtered_df is not None:
        st.sidebar.title("Generate Report:")
        select.subheader("".join(["Statistical Profile: ", file]))
        select.dataframe(filtered_df, use_container_width=True)
        px, py = cm.call_ColumnXY(filtered_df)
        for i_vars in range(len(py)):
            pfig = cm.call_scatterPlot_config(px[i_vars], py[i_vars], filtered_df.columns[i_vars])
            col_scat_plots.append(pfig)
        if pfig is not None and filtered_df is not None:
            for i_figs in range(len(col_scat_plots)):
                cm.iop.ShowPlot(select, col_scat_plots[i_figs])
    else:
        lfig = None


if __name__ == '__main__':
    data_select()
