import os
import sys
import configparser


class ProVars(object):

    def __init__(self, configFile='config/config.ini', configBase='config/config-database-privat.ini'):
        # Read Configuration
        self.inputConnectStr = None
        self.input_connect_args = None
        config = configparser.ConfigParser()
        config.read(configFile)

        # DEFAULT
        self.relDataPath = os.getcwd() + config.get('DEFAULT', 'relDataPath')
        self.model1TableName = config.get('TABLE_MODEL_1', 'Model_1_TableName')
        self.tableColumn_1 = config.get('TABLE_MODEL_1', 'Column_1')
        self.tableColumn_2 = config.get('TABLE_MODEL_1', 'Column_2')
        self.tableColumn_3 = config.get('TABLE_MODEL_1', 'Column_3')
        self.tableColumn_4 = config.get('TABLE_MODEL_1', 'Column_4')

        # DATABASE
        self.init_DB(configBase)

    def init_DB(self, configBase):
        configDatabase = configparser.ConfigParser()
        # configDatabase.read('../config/config-database.ini', encoding='utf')
        configDatabase.read(configBase, encoding='utf')

        # [ICBM-OCEAN-DATABASE]
        self.I_DB_TYPE = configDatabase.get('ICBM-OCEAN-DATABASE', 'I_DB_TYPE')
        self.I_DB_NAME = configDatabase.get('ICBM-OCEAN-DATABASE', 'I_DB_NAME')
        self.I_DB_SCHE = configDatabase.get('ICBM-OCEAN-DATABASE', 'I_DB_SCHE')
        self.I_DB_PORT = configDatabase.get('ICBM-OCEAN-DATABASE', 'I_DB_PORT')
        self.I_USER = configDatabase.get('ICBM-OCEAN-DATABASE', 'I_USER')
        self.I_PASS = configDatabase.get('ICBM-OCEAN-DATABASE', 'I_PASS')
        self.I_DB_PATH = configDatabase.get('ICBM-OCEAN-DATABASE', 'I_DB_PATH')

        # [2. Database]
        # self.O_DB_TYPE = configDatabase.get('OUTPUT-DATABASE', 'O_DB_TYPE')
        # self.O_DB_NAME = configDatabase.get('OUTPUT-DATABASE', 'O_DB_NAME')
        # self.O_DB_SCHE = configDatabase.get('OUTPUT-DATABASE', 'O_DB_SCHE')
        # self.O_DB_PORT = configDatabase.get('OUTPUT-DATABASE', 'O_DB_PORT')
        # self.O_USER = configDatabase.get('OUTPUT-DATABASE', 'O_USER')
        # self.O_PASS = configDatabase.get('OUTPUT-DATABASE', 'O_PASS')
        # self.O_DB_PATH = configDatabase.get('OUTPUT-DATABASE', 'O_DB_PATH')

        def inputDBStr():
            inputStr = self.I_DB_TYPE + "://" + self.I_USER + ":" + self.I_PASS + "@" + self.I_DB_PATH + ":" + self.I_DB_PORT + "/" + self.I_DB_NAME
            if self.I_DB_TYPE.find('postgresql') == 0:
                input_connect_args = {'options': '-csearch_path="' + self.I_DB_SCHE + '"'}
            else:
                input_connect_args = {}
            return inputStr, input_connect_args

        # def outputDBStr():
        #     outputStr = self.O_DB_TYPE + "://" + self.O_USER + ":" + self.O_PASS + "@" + self.O_DB_PATH + ":" + self.O_DB_PORT + "/" + self.O_DB_NAME
        #     if self.O_DB_TYPE.find('postgresql') == 0:
        # input_connect_args = {'options': '-csearch_path="' + self.I_DB_SCHE + '"'}
        #     else:
        #         output_connect_args = {}
        #     return outputStr, output_connect_args

        self.inputConnectStr, self.input_connect_args = inputDBStr()
        # self.outputConnectStr, self.output_connect_args = outputDBStr()


def main():
    """
    Test the ProVars class
    """
    os.chdir('../../')
    pv = ProVars()

    # INPUT
    print(pv.I_DB_TYPE)
    print(pv.I_DB_NAME)
    print(pv.I_DB_SCHE)
    print(pv.I_DB_PORT)
    print(pv.I_USER)
    print(pv.I_PASS)
    print(pv.I_DB_PATH)
    print(pv.inputConnectStr)
    print(pv.input_connect_args)

    # OUTPUT
    # print(pv.O_DB_TYPE)
    # print(pv.O_DB_NAME)
    # print(pv.O_DB_SCHE)
    # print(pv.O_DB_PORT)
    # print(pv.O_USER)
    # print(pv.O_PASS)
    # print(pv.O_DB_PATH)
    # print(pv.outputConnectStr)

    # TABEL_MODEL_1
    print(pv.model1TableName)
    print(pv.tableColumn_1)
    print(pv.tableColumn_2)
    print(pv.tableColumn_3)
    print(pv.tableColumn_4)


if __name__ == "__main__":
    main()
