import logging
import os
import sys
import pandas as pd
import sqlalchemy
from pandas.errors import DatabaseError
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import registry, sessionmaker
from sqlalchemy import create_engine, Column, Integer, Float, String, MetaData, Table, table, inspect, select


mapper_registry = registry()


class DB_ORM(object):
    """
    DB_ORM

    A class that provides methods for interacting with a database using an ORM.

    Attributes:
        dbc (Object): An object that represents the database connection.
        session (Object): An object that represents the database session.
        engine (Object): An object that represents the database engine.
        metadata (Object): An object that represents the database metadata.

    Methods:
        check_session()
            Checks if the database session is active.

        check_db_connect()
            Checks the status of the database connection.

        close_session()
            Closes the database session and disposes the engine.

        create_class(db_schema, db_table_name, db_columns, db_primary_keys)
            Creates a class dynamically based on the database table schema.

        map_to_class(schema_name, object_name)
            Maps an existing database table to a class.

        create_table(db_schema, db_table_name, db_columns, db_primary_keys)
            Creates a database table based on the provided schema.

        drop_table(class_name)
            Drops a database table.

        add_rows_to_table(class_name, keywords)
            Adds rows to a database table.

        delete_rows_from_table(class_name, condition)
            Deletes rows from a database table based on the provided condition.
    """

    def __init__(self, dbc, session):
        self.dbc = dbc
        self.session = session
        self.engine = dbc.engine
        self.metadata = dbc.meta
        pass

    # def __del__(self):
    #     try:
    #         self.session.close()
    #         self.engine.dispose()
    #     except AttributeError as err:
    #         print("Ups... an AttributeError Error occurred! {0}".format(err))

    def check_session(self):
        """
        Check if the session is active.

        :return: The status of the session (True if active, False otherwise).
        :rtype: bool
        """
        try:
            chk = self.session.is_active
            return chk
        except Exception as exception:
            logging.error(f"Failed to close session: {exception}, type: {type(exception)}")

    def check_db_connect(self):
        """
        Method to check the status of the database connection.

        :return: The status of the database connection.
        """
        try:
            status = self.engine.pool.status()
            return status
        except Exception as exception:
            logging.error(f"Failed to close connection: {exception}, type: {type(exception)}")

    def close_session(self):
        """
        Closes the session and disposes of the engine.

        :return: None
        """
        try:
            self.session.close()
            self.engine.dispose()
        except Exception as exception:
            logging.error(f"Failed to close session: {exception}, type: {type(exception)}")

    def create_class(self, db_schema, db_table_name, db_columns, db_primary_keys):
        """
        Creates a class based on a database table.

        :param db_schema: The name of the database schema.
        :param db_table_name: The name of the database table.
        :param db_columns: A dictionary containing the column names and types.
        :param db_primary_keys: A list of primary key column names.
        :return: The newly created class representing the database table.
        """
        tabname = mapper_registry.metadata.tables.get(db_table_name)
        if tabname is None:
            column_defs = [Column(name, eval(type)) for name, type in db_columns.items()]
            for column in column_defs:
                if column.name in db_primary_keys:
                    column.primary_key = True
            tabname = Table(db_table_name, mapper_registry.metadata, schema=db_schema, *column_defs)
        tab_class = type(db_table_name.title(), (object,), {})
        mapper_registry.map_imperatively(tab_class, tabname)
        return tab_class

    def map_to_class(self, schema_name, object_name):
        """
        :param schema_name: The name of the schema where the object resides.
        :param object_name: The name of the object to be mapped to a class.
        :return: The class corresponding to the object.
        """
        try:
            self.metadata.reflect(self.engine, schema=schema_name, only=[object_name])
            base = automap_base(metadata=self.metadata)
            base.prepare()
            class_name = getattr(base.classes, object_name)
            return class_name
        except Exception as exception:
            logging.warning(f"Failed to map object: {exception}, type: {type(exception)}")
            return None
            # raise exception  # For Debug purpose

    def map_to_class_2(self, schema_name, object_name):
        """
        Todo: Not in use now ,maybe useful!
        Maps an existing database table to a class.

        :param schema_name: The name of the database schema.
        :param object_name: The name of the table to be mapped.
        :return: The mapped class.
        """
        try:
            metadata = MetaData(bind=self.engine, schema=schema_name)
            metadata.reflect()
            table = metadata.tables[object_name]
            Base = automap_base(metadata=metadata)
            Base.prepare()
            mapped_class = Base.classes[object_name]
            return mapped_class
        except Exception as exception:
            logging.error(f"Failed to map table to class: {exception}, type: {type(exception)}")

    def create_table(self, db_schema, db_table_name, db_columns, db_primary_keys):
        """
        Create a table in the database.

        :param db_schema: The name of the database schema in which to create the table.
        :type db_schema: str
        :param db_table_name: The name of the table to be created.
        :type db_table_name: str
        :param db_columns: The columns and their respective data types to be included in the table.
        :type db_columns: dict
        :param db_primary_keys: The primary keys to be set for the table.
        :type db_primary_keys: list
        :return: The created table.
        :rtype: sqlalchemy.Table
        """
        check_if_exists = inspect(self.dbc.engine).has_table(db_table_name, db_schema)
        if check_if_exists is not True:
            try:
                db_table = self.create_class(db_schema, db_table_name, db_columns, db_primary_keys)
                db_table.__table__.create(bind=self.dbc.engine, checkfirst=True)
                self.session.commit()
                return db_table
            except Exception as exception:
                logging.error(f"Failed to create table: {exception}, type: {type(exception)}")
        else:
            logging.warning(f"Table '{db_table_name}' already exists.")

    # def create_FromModel(self, db_model):
    #     # self.metadata.reflect(self.engine)
    #     # base = automap_base(metadata=self.metadata)
    #     # base.prepare()
    #
    #     class_name = getattr(base.classes, db_model)
    #     print(class_name)
    #     return class_name
    #     pass

    def drop_table(self, class_name):
        """
        Drops the table associated with the given class name from the database.

        :param class_name: the name of the class representing the table
        :return: None
        """
        try:
            class_name.__table__.drop(bind=self.dbc.engine, checkfirst=True)
            self.session.commit()
        except Exception as exception:
            logging.error(f"Failed to drop table: {exception}, type: {type(exception)}")

    def add_rows_to_table(self, class_name, keywords):
        """
            Method to add rows to a table.

            :param class_name: The name of the class representing the table.
            :type class_name: class
            :param keywords: A dictionary containing keyword arguments to initialize the row.
            :type keywords: dict
            :return: A boolean indicating whether the rows were successfully added.
            :rtype: bool
        """
        try:
            row = class_name(**keywords)
            executed = bool(self.session.add(row))
            self.session.commit()
            return executed
        except Exception as exception:
            logging.error(f"Failed to add rows: {exception}, type: {type(exception)}")

    def delete_rows_from_table(self, class_name, condition):
        """
        :param class_name: The name of the table/class from which rows need to be deleted.
        :param condition: The condition to filter the rows to be deleted.
        :return: True if the rows were successfully deleted, False otherwise.

        Deletes rows from a specific table based on the given condition.
        """
        # # Delete specific dataset (row) by id
        try:
            executed = bool(self.session.query(class_name).filter(condition).delete())
            self.session.commit()
            return executed
        except Exception as exception:
            logging.error(f"Failed to delete rows: {exception}, type: {type(exception)}")

    def store_in_DB(self, dataframe, tableName, schema, idxName='id'):
        try:
            # SenData = Table(tableName, self.meta_raw, autoload=True, autoload_with=self.engine_raw)
            # select_st = select([SenData])
            # df = pd.read_sql(select_st, self.dbc_raw)
            # dataframe = pd.read_sql_table(tableName, con=self.dbc_raw)
            print('stored :', tableName)
            dataframe.to_sql(tableName, schema=schema, con=self.dbc.engine, index=True, index_label=idxName, if_exists='replace')
            # dataframe.to_sql(tableName, schema=schema, con=self.dbc.engine, index=False, if_exists='replace')
            return True
        except DatabaseError as DB_exception:
            logging.error(f"Failed to store table: {DB_exception}, type: {type(DB_exception)}")
            return False


if __name__ == '__main__':
    pass
