import logging

import pandas as pd
import sqlalchemy
import sqlalchemy as sq
from sqlalchemy import Integer, String
from sqlalchemy.exc import DatabaseError
from sqlalchemy.orm import sessionmaker

from dBaseOp.provar import ProVars as pv
from divtools.divTools import Tools as TOOLS


class DatabaseConnection(pv, object):
    def __init__(self):
        """
        Constructor
        """
        super().__init__()
        self.tools = TOOLS()
        # self.DIO = DataIO()

        # Database
        try:
            if self.I_DB_TYPE.find('sqlite') == 0:
                self.engine = sq.create_engine("sqlite://")
                @sq.event.listens_for(self.engine, "first_connect")
                def schema_attach(dbapi_connection, connection_record):
                    dbapi_connection.execute(
                        f"ATTACH DATABASE '{self.I_DB_NAME}.db' AS {self.I_DB_NAME}"
                    )
            else:
                self.engine = sq.create_engine(self.inputConnectStr, connect_args=self.input_connect_args, pool_size=20,
                                                max_overflow=30, pool_recycle=3600)
            self.dbc = self.engine.connect()
            print(f"Connected to: {self.engine.url}")
            self.meta = sq.MetaData()
            # self.meta = db.MetaData(schema='icbm4uwd')
            self.Session = sessionmaker(bind=self.engine)
        except ConnectionError as Con_Exception:
            logging.error(f"Open connection failed: {Con_Exception}, type: {type(Con_Exception)}")
            print("Ups... a ConnectionError  Error occurred! {0}".format(Con_Exception))
            # raise

    # def __init__(self, username, password, host, port, database):
    #     self.engine = create_engine(f'postgresql://{username}:{password}@{host}:{port}/{database}', echo=True)
    #     self.meta = sqlalchemy.MetaData()
    #     self.Session = sessionmaker(bind=self.engine)

    def __del__(self):
        try:
            self.Session.close_all()
            self.engine.dispose()
        except ConnectionError as Con_Exception:
            logging.error(f"Closing connection failed: {Con_Exception}, type: {type(Con_Exception)}")
            # print("Ups... an ConnectionError Error occurred! {0}".format(Con_Exception))
            # raise

    def getTableNames(self):
        tableNames = self.meta.tables.values()
        return tableNames

    # def addTable(self, tableName):
    #     self.Session.add(tableName)
    #     self.Session.object_session.commit()

    def create_session(self):
        return self.Session()

    def close_session(self):
        self.Session.close_all()

    # def create_tables(self):
    #     Base.metadata.create_all(self.engine)
    #
    # def drop_tables(self):
    #     Base.metadata.drop_all(self.engine)


class dbBasicOperations(object):
    def __init__(self, session, meta, engine):
        self.session = session
        self.meta = meta
        self.engine = engine
        pass

    def read_table(self, table_name):
        return pd.read_sql_table(table_name, self.engine)

    def write_table(self, table_name, df):
        df.to_sql(table_name, self.engine)

    def read_csv(self, file_path):
        return pd.read_csv(file_path)

    def write_csv(self, file_path, df):
        df.to_csv(file_path, index=False)

    def execute_query(self, query):
        return pd.read_sql_query(query, self.engine)

    # def create_table(self, table_name, columns):
    #     table = sq.Table(
    #         table_name, self.meta,
    #         *[Column(name, self.determine_type(type)) for name, type in columns.items()],
    #     )
    #     self.meta.create_all(self.engine)
    #     return table
    def listAllSchema(self):
        insp = sq.inspect(self.engine)
        db_list = insp.get_schema_names()
        print(db_list)

    def createSchema(self, schema_name):
        from sqlalchemy.sql.ddl import CreateSchema
        with self.engine.connect() as conn:
            result = conn.execute(CreateSchema(schema_name))
        return result


    def create_table(self, table_name, columns):
        table = sqlalchemy.Table(table_name, self.meta, schema='io_mass_spec', include_columns=columns)
        table.create(self.engine)

    def delete_table(self, table_name):
        table = sqlalchemy.Table(table_name, self.meta)
        table.drop(self.engine)

    @staticmethod
    def determine_type(type_str):
        if type_str == 'int':
            return Integer
        elif type_str == 'str':
            return String
        else:  # If type is not recognized, use Text by default
            return sqlalchemy.Text


if __name__ == "__main__":
    pass
